#!/usr/bin/python

from scanner import *
from ast import *

precedence = (
    ('left','OR'),
    ('left','AND'),
    ('right','NOT'),
    ('nonassoc', 'EQ', 'NEQ', 'GEQ', 'LEQ', 'GT', 'LT'),
    ('left','PLUS','MINUS'),
    ('left','TIMES','BY')
)

def p_program(p):
    '''program : agent modules awdecls ardecls locdecls events'''
    p[0] = pgmAst(p[1],p[2],p[3],p[4],p[5],p[6])


def p_agent(p):
    '''agent : AGENT CID NL'''
    p[0] = p[2]


def p_modules_empty(p):
    '''modules : empty'''
    p[0] = []
def p_modules_non_empty(p):
    '''modules : module modules'''
    p[0] = [ p[1] ] + p[2]

def p_module(p):
    '''module : USING MODULE CID COLON NL INDENT actuatordecls sensordecls DEDENT'''
    p[0] = moduleAst(p[3],p[7],p[8])


def p_actuatordecls_empty(p):
    '''actuatordecls : empty'''
    p[0] = []

def p_actuatordecls_nonempty(p):
    '''actuatordecls : ACTUATORS COLON NL INDENT decls DEDENT'''
    #TODO What is the scope for the declarations?
    p[0] = p[5]


def p_sensordecls_empty(p):
    '''sensordecls : empty'''
    p[0] = []

def p_sensordecls(p):
    '''sensordecls : SENSORS COLON NL INDENT decls DEDENT'''
    #TODO What is the scope for the declarations?
    p[0] = p[5]


def p_awdecls_empty(p):
    '''awdecls : empty'''
    p[0] = []

def p_awdecls_nonempty(p):
    '''awdecls : ALLWRITE COLON NL INDENT decls DEDENT'''
    for decl in p[5]:
        decl.set_scope(MULTI_WRITER)
    p[0] = p[5]


def p_ardecls_empty(p):
    '''ardecls : empty'''
    p[0] = []

def p_ardecls_nonempty(p):
    '''ardecls : ALLREAD COLON NL INDENT rvdecls DEDENT'''
    for decl in p[5]:
        decl.set_scope(MULTI_READER)
    p[0] = p[5]


def p_locdecls_empty(p):
    '''locdecls : empty'''
    p[0] = []

def p_locdecls_nonempty(p):
    '''locdecls : LOCAL COLON NL INDENT decls DEDENT'''
    p[0] = p[5]

def p_decls_empty(p):
    '''decls : empty
             | pass'''
    p[0] = []

def p_decls_nonempty(p):
    '''decls : decl decls'''
    p[0] = [ p[1] ] + p[2]


def p_decl_1(p):
    '''decl : type varname NL'''
    p[0] = declAst(p[1],p[2])
def p_decl_2(p):
    '''decl : type varname ASGN exp NL'''
    p[0] = declAst(p[1],p[2],p[4])

# TODO Support Map type
#def p_decl_3(p):
#    '''decl : mapdecl NL'''
#    p[0] = p[1]
#
#def p_mapdecl(p):
#    '''mapdecl : MAP LT type COMMA type GT varname'''
#    p[0] = []

def p_rvdecls_empty(p):
    '''rvdecls : empty
               | pass'''
    p[0] = []

def p_rvdecls_nonempty(p):
    '''rvdecls : rvdecl rvdecls'''
    p[0] = [ p[1] ] + p[2]

def p_rvdecl(p) :
    '''rvdecl : type varname LBRACE owner RBRACE NL
              | type varname LBRACE owner RBRACE ASGN num NL

    '''
    p[0] = [] #TODO specify owner of the allread variables

def p_owner(p) :
    '''owner : TIMES
             | INUM'''
    p[0] = [] #TODO Get owner id

def p_funccall(p):
    '''funccall : varname LPAR args RPAR'''
    p[0] = funcAst(p[1],p[3])

def p_args_1(p):
    '''args : empty'''
    p[0] = []
def p_args_2(p):
    '''args : neargs'''
    p[0] = p[1]

def p_neargs_1(p):
    '''neargs : exp'''
    p[0] = [ p[1] ]
def p_neargs_2(p):
    '''neargs : exp COMMA neargs'''
    p[0] = [ p[1] ] + p[3]

#def p_varnames_1(p):
#    '''varnames : varname'''
#    p[0] = [ p[1] ]
#def p_varnames_2(p):
#    '''varnames : varname COMMA varnames'''
#    p[0] = [ p[1] ] + p[3]

def p_type(p):
    '''type : numtype
            | uncertainnumtype
            | STRING
    '''
    p[0] = p[1]

def p_uncertainnumtype(p):
    '''uncertainnumtype : UNCERTAIN LT numtype GT'''
    # TODO construct AST with uncertain information
    p[0] = "uncertain<" + p[3] + ">"

def p_numtype(p):
    '''numtype : INT
               | FLOAT
               | IPOS
               | BOOLEAN
    '''
    p[0] = p[1]


def p_events_1(p):
    '''events : empty'''
    p[0] = []
def p_events_2(p):
    '''events : event events'''
    p[0] = [ p[1] ] + p[2]

def p_event(p):
    '''event : eventdef'''
    p[0] = p[1]

def p_event_urgent(p):
    '''event : URGENT eventdef'''
    p[0] = p[2]

def p_eventdef(p):
    '''eventdef : LID COLON NL INDENT PRE COLON cond NL effblock DEDENT'''
    p[0] = eventAst(p[1],p[7],p[9])

def p_effblock_1(p):
    '''effblock : EFF COLON stmt'''
    p[0] = [ p[3] ]
def p_effblock_2(p):
    '''effblock : EFF COLON NL INDENT stmts DEDENT'''
    p[0] = p[5]

def p_cond(p):
    '''cond : cond_exp'''
    p[0] = p[1]

def p_stmts_1(p):
    '''stmts : empty'''
    p[0] = []
def p_stmts_2(p):
    '''stmts : stmt stmts'''
    p[0] = [ p[1] ] + p[2]

def p_stmt_1(p):
    '''stmt : asgn
            | pass
            | funccall NL
            | modulefunccall NL
    '''
    p[0] = p[1]
def p_stmt_2(p):
    '''stmt : ATOMIC COLON NL INDENT stmts DEDENT'''
    p[0] = atomicAst(p[5])
def p_stmts_3(p):
    '''stmt : IF cond COLON NL INDENT stmts DEDENT elseblock'''
    p[0] = iteAst(p[2],p[6],p[8])

def p_modulefunccall(p):
    '''modulefunccall : CID LPAR args RPAR'''
    p[0] = mfAst(p[3])

def p_elseblock(p):
    '''elseblock : ELSE COLON NL INDENT stmts DEDENT'''
    p[0] = p[5]

def p_pass(p):
    '''pass : PASS NL'''
    p[0] = passAst()

def p_asgn(p):
    '''asgn : varname ASGN exp NL'''
    p[0] = asgnAst(p[1],p[3])


def p_exp(p):
    '''exp : cond_exp'''
    p[0] = p[1]

def p_cond_exp_1(p):
    '''cond_exp : rel_exp'''
    p[0] = p[1]
def p_cond_exp_2(p):
    '''cond_exp : NOT cond_exp'''
    p[0] = exprAst('cond',p[2],None,p[1])
def p_cond_exp_3(p):
    '''cond_exp : cond_exp AND cond_exp
                | cond_exp OR cond_exp
    '''
    p[0] = exprAst('cond',p[1],p[3],p[2])

def p_rel_exp_1(p):
    '''rel_exp : arith_exp'''
    p[0] = p[1]
def p_rel_exp_2(p):
    '''rel_exp : arith_exp relop arith_exp'''
    p[0] = exprAst('rel',p[1],p[3],p[2])

def p_arith_exp_1(p):
    '''arith_exp : unary_exp'''
    p[0] = p[1]
def p_arith_exp_2(p):
    '''arith_exp : arith_exp PLUS arith_exp
                 | arith_exp TIMES arith_exp
                 | arith_exp MINUS arith_exp
                 | arith_exp BY arith_exp
    '''
    p[0] = exprAst('arith',p[1],p[3],p[2])

def p_unary_exp_1(p):
    '''unary_exp : primary_exp'''
    p[0] = p[1]
def p_unary_exp_2(p):
    '''unary_exp : PLUS unary_exp
                 | MINUS unary_exp
    '''
    p[0] = exprAst('unary',p[2],None,p[1])

def p_primary_exp_1(p):
    '''primary_exp : varname
                   | bval
                   | num
                   | funccall
    '''
    p[0] = p[1]
def p_primary_exp_2(p):
    '''primary_exp : LPAR exp RPAR'''
    p[0] = p[2]

def p_bval(p):
    '''bval : TRUE
            | FALSE
    '''
    p[0] = exprAst('bval',p[1])

def p_num(p):
    '''num : INUM
           | FNUM
    '''
    p[0] = exprAst('num',p[1])

def p_varname(p):
    '''varname : LID'''
    p[0] = exprAst('var',p[1])

def p_relop(p):
    '''relop : EQ
             | NEQ
             | GEQ
             | LEQ
             | GT
             | LT
    '''
    p[0] = p[1]

def p_empty(p):
    '''empty :'''
    pass

def p_error(p):
    print("syntax error in input on line ",p.lineno,p.type)



class KoordParser(object):
    def __init__(self,lexer=None):
        self.lexer = IndentLexer()
        self.parser = yacc.yacc()


    def parse(self,code):
        self.lexer.input(code)
        result = self.parser.parse(lexer=self.lexer)
        return result
