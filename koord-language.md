# Koord Language References

## Koord Program Structure

A koord program looks like this :

```
agent <agentname>

using module <modulename> :
    actuators :
         <type> <actuatornames>
         <type> <actuatornames>
  	 ...
    sensors :
	 <type> <sensornames>
	 <type> <sensornames>
         ...

using module ...
...

import <declfile>  (for shared declarations)

<type> <variablenames>
<type> <variablenames>
...


init:
    <statements>


<eventname> :
    pre : <condition>
    eff :
        <statements>
```
where :

1. declfile is the file which contains shared declarations.
2. the module is a predeclared module, and a user can only use the sensors and actuators declared in the module. They do not have to use all available sensors and actuators.
3. statements : assignments, if then else, loops, functioncalls.
4. variablenames, actuatornames, etc are comma separated parameters.

The format of the declfile is as follows :

```
allwrite :
   <type> <variablenames>
   ...

allread :
   <type>[<owners>] <variablename>
   ...
```
where owner can be an integer corresponding to the id of the owner agent, or "\*" when there is a variable corresponding to each agent.

The scanning process involves filters applied at various levels. our language uses indentations to indicate blocks.


## LANGUAGE FEATURES

1. Event driven:  
   An Event has a precondition and an effect. Precondition being true may or may not result in event occuring. Precondition being false means event will never occur. In the StarL simulator, an event with a true precondition occurs if its precondition is checked before any other event . In the Koord framework, this is uncertain . For more details, look up the papers section on cyphyhouse.github.io .

2. Atomic:  
   Atomic is a convenient keyword to express locking of shared variables. any code block within an atomic statement is executed together. The simulator tries to get locks on all the shared variables involved in the statements in the atomic block. If it fails, the loop restarts. see AddNums and LeaderElect to illustrate.

3. Trivial modules:  
   If the example is not using any blackboxes for dynamic behavior , the trivial module is used as a syntactic placeholder. Similarly, if there are no shared declarations, we use trivial declarations. see trivialexample.krd

4. Special variables:  
   These variables are predefined.  
   `int pid` : robot id  
   `now` : current time  
   `int numAgents` : number of participants in the protocol.

5. Special Functions:  
   We have a few predefined libraries and fucntions, as we currently do not fully support function declaration. We can only support easy function declarations.

6. New variable types:  
   ItemPosition = (x,y,z) coordinates.  
   ObstacleList = list of obstacles defined by convex hull of points.
