import os
import unittest

from frontend.parser import KoordParser

class KoordParserTestCase(unittest.TestCase):
    def setUp(self):
        self.parser = KoordParser()

    @staticmethod
    def readCode(filename):
        abs_filename = os.path.join(os.path.dirname(__file__), filename)
        with open(abs_filename, 'r') as f:
            return f.read()

    def testKoordParser_HVAC(self):
        filename = 'hvac/hvac.krd'
        pgm = self.parser.parse(KoordParserTestCase.readCode(filename))
        self.assertEqual(pgm.name, "HVAC")
        # TODO Assert on AST

    def testKoordParser_HVAC2(self):
        filename = 'hvac2/hvac2.krd'
        pgm = self.parser.parse(KoordParserTestCase.readCode(filename))
        self.assertEqual(pgm.name, "HVAC2")
        # TODO Assert on AST

    def testKoordParser_AddNums(self):
        filename = "addNums/addNums.krd"
        pgm = self.parser.parse(KoordParserTestCase.readCode(filename))
        self.assertEqual(pgm.name, "AddNums")
        # TODO Assert on AST

    def testKoordParser_LeaderElect(self):
        filename = "leaderElect/leaderElect.krd"
        pgm = self.parser.parse(KoordParserTestCase.readCode(filename))
        self.assertEqual(pgm.name, "LeaderElect")
        # TODO Assert on AST

    def testKoordParser_Fischer(self):
        filename = "fischers/fischers.krd"
        pgm = self.parser.parse(KoordParserTestCase.readCode(filename))
        self.assertEqual(pgm.name, "Fischer")
        # TODO Assert on AST
