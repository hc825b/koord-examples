# Steps to translate hvac.krd to hvac.psi

## Create PSI data abstraction for Agent specification

+ Module Therm -> `dat KOORD_Therm`
      - Sensors: `temp`
      - Actuators: `state`
      - Constructor(init): None
+ Agent HVAC -> `dat KOORD_HVAC`
      - using module Therm: `mod_Therm`  
            * Each agent instance has its own module instance
      - Local Variables: `min` `max`
      - Constructor(init):  
            * Create an instance of used module
            * Assign initial values
      - Events: `hvaccontrol`  
            * Define a function with the name
            * Copy and paste **eff** of the event
            * Replace variable names accordingly

## Declare PSI variables

+ Declare Environment state variables
      - `ENV_State.state` `ENV_State.temp`
+ Declare shared variables
      - Allread: none
      - Allwrite: none
+ Declare an instance for each agent
      - `koord_HVAC := KOORD_HVAC()`

## Process a sequence of Program turn events starting from given Program states

+ Read Environment state
+ Restore Program states and read sensors
+ Write assertions over states and sensor values
+ For each event in the give sequence of events
      - observe the **pre**
      - Call the function translated from the event
+ Record current Program states
+ Copy actuator values to Environment
