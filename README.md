# README #

### What is this repository for? ###

* This repository hosts example Koord programs modelling Cyber-physical systems
  and a Python PLY parser building Abstract Syntax Tree

### How do I get set up? ###

* Summary of set up
  + Install Python 2.7 and `pip`
```
sudo apt install python python-pip # for Ubuntu 16.04 LTS
```
  + Install required Python modules using `pip`
```
pip install -r requirements.txt
```

* How to run tests

```
python -m unittest discover
```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Chiao Hsieh <chsieh16@illinois.edu>
